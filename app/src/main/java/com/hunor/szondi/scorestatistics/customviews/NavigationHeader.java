package com.hunor.szondi.scorestatistics.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Custom navigation header used in the app.
 * Created by Szondi Hunor on 2/12/2019.
 */
public class NavigationHeader extends ConstraintLayout {

    @BindView(R.id.navigationHeaderButton)
    ImageView button;

    @BindView(R.id.navigationHeaderTitleTextView)
    TextView title;

    public NavigationHeader(Context context) {
        super(context);
    }

    public NavigationHeader(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.navigation_header_view, this);
        ButterKnife.bind(this);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.NavigationHeader, 0, 0);
        String titleText = attributes.getString(R.styleable.NavigationHeader_title);
        if (titleText == null) {
            title.setText(ResourceUtil.getInstance().getString(R.string.title));
        } else {
            title.setText(titleText);
        }
        attributes.recycle();
    }

    public NavigationHeader(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Sets the visibility of the navigation header title
     * @param visibility Has to be a View.VISIBLE/View.INVISIBLE/View.GONE
     * @return navigation header
     */
    public NavigationHeader setTitleVisibility(int visibility) {
        title.setVisibility(visibility);
        return this;
    }

    /**
     * Sets the navigation header title
     * @param text any text
     * @return navigation header
     */
    public NavigationHeader setTitle(String text) {
        title.setText(text);
        return this;
    }

    /**
     * Sets the visibility of the navigation header button
     * @param visibility Has to be a View.VISIBLE/View.INVISIBLE/View.GONE
     * @return navigation header
     */
    public NavigationHeader setButtonVisibility(int visibility) {
        button.setVisibility(visibility);
        return this;
    }

    /**
     * Sets listener for the navigation header button
     * @param listener Has to be an instance of View.OnClickListener
     * @return navigation header
     */
    public NavigationHeader setButtonClickListener(OnClickListener listener) {
        button.setOnClickListener(listener);
        return this;
    }

    /**
     * Sets the icon of the navigation header button
     * @param id Has to be a drawable resource ex.: R.drawable.id
     * @return navigation header
     */
    public NavigationHeader setButtonIcon(int id) {
        button.setImageDrawable(ResourceUtil.getInstance().getDrawable(id));
        return this;
    }
}
