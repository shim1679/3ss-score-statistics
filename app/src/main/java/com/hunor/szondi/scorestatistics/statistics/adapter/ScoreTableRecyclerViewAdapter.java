package com.hunor.szondi.scorestatistics.statistics.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hunor.szondi.scorestatistics.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter for score table recycler view in StatisticsFragment
 * Created by Szondi Hunor on 2/14/2019.
 */
public class ScoreTableRecyclerViewAdapter extends RecyclerView.Adapter {

    private ArrayList<String> data;

    public ScoreTableRecyclerViewAdapter(ArrayList<String> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.score_table_recyclerview_viewholder, parent, false);
        return new ScoreTableRecyclerViewAdapter.ScoreTableViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TextView field = ((ScoreTableViewHolder) holder).contentTextView;
        field.setText(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ScoreTableViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contentTextView)
        TextView contentTextView;

        ScoreTableViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
