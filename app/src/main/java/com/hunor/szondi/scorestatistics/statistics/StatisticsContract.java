package com.hunor.szondi.scorestatistics.statistics;

import com.hunor.szondi.scorestatistics.base.IPresenter;
import com.hunor.szondi.scorestatistics.base.IView;
import com.hunor.szondi.scorestatistics.models.TeamModel;

import java.util.ArrayList;

/**
 * Creates connection between View and Presenter.
 * Created by Szondi Hunor on 2/12/2019.
 */
public interface StatisticsContract {

    interface View extends IView {

        /**
         * Builds the statistics table from stats data
         * @param stats data
         */
        void initStatisticsTable(ArrayList<TeamModel> stats);

        /**
         * Updates the statistics table from stats data
         * @param stats data
         */
        void updateStatisticsTable(ArrayList<TeamModel> stats);


        /**
         * Builds the score table from the data and number of columns
         * @param scoreTable data
         * @param size number of columns
         */
        void initScoreTable(ArrayList<String> scoreTable, int size);

        /**
         * Builds sort dropdown list from data provided
         * @param list data
         */
        void initSortSpinner(ArrayList<String> list);
    }

    interface Presenter<T extends View> extends IPresenter<T> {

        /**
         * Generates the data for the statistics and score table from
         * the one is stored in shared preferences
         */
        void prepareData();

        /**
         * Servers the sorting options for the dropdown list
         */
        void initSortSpinner();

        /**
         * Sorts the statistics tables data
         * @param option sorts by this given int option
         */
        void sortStatsBy(int option);
    }
}
