package com.hunor.szondi.scorestatistics.addteam;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.base.BaseDialogFragment;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Provides a UI for adding new teams
 * Created by Szondi Hunor on 2/13/2019.
 */
public class AddTeamDialog extends BaseDialogFragment implements AddTeamContract.View {

    public static String TAG = AddTeamDialog.class.getCanonicalName();

    @BindView(R.id.addTeamDialogEditText)
    EditText teamNameEditText;

    @BindView(R.id.addTeamDialogError)
    TextView error;

    private AddTeamContract.Presenter<AddTeamContract.View> mPresenter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_team_dialog, container, false);
        ButterKnife.bind(this, view);

        mPresenter = AddTeamPresenter.getInstance(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @OnClick(R.id.addTeamDialogSaveButton)
    void onSaveClicked() {
        if (teamNameEditText.getText().toString().isEmpty()) {
            error.setText(ResourceUtil.getInstance().getString(R.string.please_enter_a_team_name));
            error.setVisibility(View.VISIBLE);
        } else {
            mPresenter.saveData(teamNameEditText.getText().toString());
        }
    }

    @Override
    public void closeDialog() {
        Toast.makeText(getContext(), getString(R.string.team_saved), Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void showErrorLabel(String message) {
        error.setText(message);
        error.setVisibility(View.VISIBLE);
    }
}
