package com.hunor.szondi.scorestatistics.utils;

import android.content.Context;
import android.content.Intent;

import com.hunor.szondi.scorestatistics.MainActivity;
import com.hunor.szondi.scorestatistics.addscore.AddScoreDialog;
import com.hunor.szondi.scorestatistics.addteam.AddTeamDialog;
import com.hunor.szondi.scorestatistics.customviews.NavigationHeader;
import com.hunor.szondi.scorestatistics.options.OptionsFragment;
import com.hunor.szondi.scorestatistics.statistics.StatisticsFragment;

import androidx.fragment.app.FragmentManager;

/**
 * Helps in the navigation between activities and fragments
 * Created by Szondi Hunor on 2/12/2019.
 */
public class NavigationHelperUtil {

    public static Intent getMainActivityIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    public static void openOptionsFragment(FragmentManager fragmentManager, int container,
                                           NavigationHeader navigationHeader) {

        OptionsFragment optionsFragment = OptionsFragment.newInstance(navigationHeader, container);

        fragmentManager.beginTransaction()
                .replace(container, optionsFragment)
                .addToBackStack(OptionsFragment.TAG)
                .commit();
    }

    public static void openStatisticsFragment(FragmentManager fragmentManager, int container,
                                              NavigationHeader navigationHeader) {

        StatisticsFragment statisticsFragment = StatisticsFragment.newInstance(navigationHeader);

        fragmentManager.beginTransaction()
                .replace(container, statisticsFragment)
                .addToBackStack(StatisticsFragment.TAG)
                .commit();
    }

    public static void openAddTeamDialog(FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            AddTeamDialog addTeamDialog = new AddTeamDialog();
            addTeamDialog.show(fragmentManager, AddTeamDialog.TAG);
        }
    }

    public static void openAddScoreDialog(FragmentManager fragmentManager) {
        if (fragmentManager != null) {
            AddScoreDialog addScoreDialog = new AddScoreDialog();
            addScoreDialog.show(fragmentManager, AddScoreDialog.TAG);
        }
    }

}
