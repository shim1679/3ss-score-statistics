package com.hunor.szondi.scorestatistics.addscore;

import com.hunor.szondi.scorestatistics.base.BasePresenter;
import com.hunor.szondi.scorestatistics.models.ScoreModel;
import com.hunor.szondi.scorestatistics.utils.DataUtil;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Logical part for AddScoreDialog
 * Created by Szondi Hunor on 2/13/2019.
 */
public class AddScorePresenter extends BasePresenter<AddScoreContract.View>
        implements AddScoreContract.Presenter<AddScoreContract.View> {

    private static AddScorePresenter presenter;

    private ArrayList<String> teamList;

    private AddScorePresenter(AddScoreContract.View view) {
        mView = view;
    }

    static AddScorePresenter getInstance(AddScoreContract.View view) {
        if (presenter == null) {
            presenter = new AddScorePresenter(view);
        } else {
            presenter.onAttach(view);
        }
        return presenter;
    }

    @Override
    public void fetchTeams() {
        teamList = DataUtil.getInstance().getAllTeams();
        Collections.sort(teamList);
        if (isViewAttached()) {
            mView.initView(teamList);
        }
    }

    @Override
    public ArrayList<String> getTeamListExceptOne(String exception) {
        ArrayList<String> copyList = new ArrayList<>(teamList);
        for (String team : copyList) {
            if (team.equals(exception)) {
                copyList.remove(team);
                break;
            }
        }
        Collections.sort(copyList);
        return copyList;
    }

    @Override
    public void saveData(String firstTeam, String secondTeam, int firstScore, int secondScore) {
        ScoreModel score = new ScoreModel()
                .setTeam1(firstTeam)
                .setTeam2(secondTeam)
                .setScore1(firstScore)
                .setScore2(secondScore);

        DataUtil.getInstance().addScore(score);

        if (isViewAttached()) {
            mView.closeDialog();
        }
    }
}
