package com.hunor.szondi.scorestatistics.addteam;

import com.hunor.szondi.scorestatistics.base.IPresenter;
import com.hunor.szondi.scorestatistics.base.IView;


/**
 * Creates connection between View and Presenter.
 * Created by Szondi Hunor on 2/15/2019.
 */
public interface AddTeamContract {

    interface View extends IView {

        /**
         * Leaves a messages and closes dialog
         */
        void closeDialog();

        /**
         * Sets the visibility of the error label to visible and changes its text to the given one
         * @param error given error message
         */
        void showErrorLabel(String error);
    }

    interface Presenter<T extends View> extends IPresenter<T> {

        /**
         * Saves a new team
         * @param team team name
         */
        void saveData(String team);
    }
}
