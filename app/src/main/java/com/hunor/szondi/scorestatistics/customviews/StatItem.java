package com.hunor.szondi.scorestatistics.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hunor.szondi.scorestatistics.R;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * View used as an item(row) of the statistics table
 * Created by Szondi Hunor on 2/14/2019.
 */
public class StatItem extends LinearLayout {

    @BindView(R.id.statItemName)
    TextView name;

    @BindView(R.id.statItemPlayedMatch)
    TextView playedMatch;

    @BindView(R.id.statItemWin)
    TextView win;

    @BindView(R.id.statItemDraw)
    TextView draw;

    @BindView(R.id.statItemLose)
    TextView lose;

    @BindView(R.id.statItemScoredGoals)
    TextView scoredGoals;

    @BindView(R.id.statItemConcededGoals)
    TextView concededGoals;

    public StatItem(Context context) {
        super(context);
    }

    public StatItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        inflate(getContext(), R.layout.stat_recyclerview_item, this);
        ButterKnife.bind(this);
    }

    public StatItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public String getName() {
        return name.getText().toString();
    }

    public StatItem setName(String name) {
        this.name.setText(name);
        return this;
    }

    public String getPlayedMatch() {
        return playedMatch.getText().toString();
    }

    public StatItem setPlayedMatch(String playedMatch) {
        this.playedMatch.setText(playedMatch);
        return this;
    }

    public String getWin() {
        return win.getText().toString();
    }

    public StatItem setWin(String win) {
        this.win.setText(win);
        return this;
    }

    public String getDraw() {
        return draw.getText().toString();
    }

    public StatItem setDraw(String draw) {
        this.draw.setText(draw);
        return this;
    }

    public String getLose() {
        return lose.getText().toString();
    }

    public StatItem setLose(String lose) {
        this.lose.setText(lose);
        return this;
    }

    public String getScoredGoals() {
        return scoredGoals.getText().toString();
    }

    public StatItem setScoredGoals(String scoredGoals) {
        this.scoredGoals.setText(scoredGoals);
        return this;
    }

    public String getConcededGoals() {
        return concededGoals.getText().toString();
    }

    public StatItem setConcededGoals(String concededGoals) {
        this.concededGoals.setText(concededGoals);
        return this;
    }
}
