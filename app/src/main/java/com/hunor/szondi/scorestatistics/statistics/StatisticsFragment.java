package com.hunor.szondi.scorestatistics.statistics;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.base.BaseFragment;
import com.hunor.szondi.scorestatistics.customviews.NavigationHeader;
import com.hunor.szondi.scorestatistics.models.TeamModel;
import com.hunor.szondi.scorestatistics.statistics.adapter.ScoreTableRecyclerViewAdapter;
import com.hunor.szondi.scorestatistics.statistics.adapter.StatRecyclerViewAdapter;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Statistics screen. This will be shown if on the options screen
 * the "Show statistics" option was selected.
 *
 * Created by Szondi Hunor on 2/12/2019.
 */
public class StatisticsFragment extends BaseFragment implements StatisticsContract.View {

    public static String TAG = StatisticsFragment.class.getCanonicalName();

    @BindView(R.id.scoreTableRecyclerView)
    RecyclerView scoreTable;

    @BindView(R.id.statisticsRecyclerView)
    RecyclerView statisticsTable;

    @BindView(R.id.sortSpinner)
    Spinner sortSpinner;

    private NavigationHeader navigationHeader;
    private StatisticsContract.Presenter<StatisticsContract.View> mPresenter;

    private StatRecyclerViewAdapter statRecyclerViewadapter;

    public static StatisticsFragment newInstance(NavigationHeader navigationHeader) {
        StatisticsFragment fragment = new StatisticsFragment();
        fragment.setNavigationHeader(navigationHeader);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = StatisticsPresenter.getInstance(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);
        ButterKnife.bind(this, view);
        mPresenter.prepareData();
        mPresenter.initSortSpinner();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        navigationHeader.setTitleVisibility(View.VISIBLE)
                .setTitle(ResourceUtil.getInstance().getString(R.string.statistics))
                .setButtonVisibility(View.VISIBLE)
                .setButtonClickListener(v -> closeFragment());
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack(TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void initStatisticsTable(ArrayList<TeamModel> stats) {
        if (getContext() == null) {
            return;
        }
        statisticsTable.setHasFixedSize(true);
        statisticsTable.setLayoutManager(new LinearLayoutManager(getContext()));
        statRecyclerViewadapter = new StatRecyclerViewAdapter(stats);
        statisticsTable.setAdapter(statRecyclerViewadapter);
    }

    @Override
    public void updateStatisticsTable(ArrayList<TeamModel> stats) {
        statRecyclerViewadapter.updateData(stats);
    }

    @Override
    public void initScoreTable(ArrayList<String> scoreTableData, int size) {
        if (getContext() == null) {
            return;
        }
        scoreTable.setHasFixedSize(true);
        scoreTable.setLayoutManager(new GridLayoutManager(getContext(), size, RecyclerView.HORIZONTAL, false));
        ScoreTableRecyclerViewAdapter scoreTableRecyclerViewAdapter = new ScoreTableRecyclerViewAdapter(scoreTableData);
        scoreTable.setAdapter(scoreTableRecyclerViewAdapter);
    }

    @Override
    public void initSortSpinner(ArrayList<String> list) {
        if (getContext() == null) {
            return;
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_list_item_1, list);
        sortSpinner.setAdapter(arrayAdapter);
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPresenter.sortStatsBy(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void setNavigationHeader(NavigationHeader navigationHeader) {
        this.navigationHeader = navigationHeader;
    }
}
