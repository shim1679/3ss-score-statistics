package com.hunor.szondi.scorestatistics.addscore;

import com.hunor.szondi.scorestatistics.base.IPresenter;
import com.hunor.szondi.scorestatistics.base.IView;

import java.util.ArrayList;

/**
 * Creates connection between View and Presenter.
 * Created by Szondi Hunor on 2/13/2019.
 */
public interface AddScoreContract {

    interface View extends IView {

        /**
         * Fills the dropdown lists with team names
         * @param teamList data
         */
        void initView(ArrayList<String> teamList);

        /**
         * Leaves a messages and closes dialog
         */
        void closeDialog();
    }

    interface Presenter<T extends View> extends IPresenter<T> {

        /**
         * Provides team names
         */
        void fetchTeams();

        /**
         * Provideseam names except one given name
         * @param exception exception
         * @return team name list
         */
        ArrayList<String> getTeamListExceptOne(String exception);

        /**
         * Saves new score to shared preferences
         * @param firstTeam firstTeam
         * @param secondTeam secondTeam
         * @param firstScore firstScore
         * @param secondScore secondScore
         */
        void saveData(String firstTeam, String secondTeam, int firstScore, int secondScore);
    }
}
