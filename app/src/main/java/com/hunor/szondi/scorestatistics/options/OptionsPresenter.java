package com.hunor.szondi.scorestatistics.options;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.base.BasePresenter;
import com.hunor.szondi.scorestatistics.utils.DataUtil;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

/**
 * Logical part for OptionFragment
 * Created by Szondi Hunor on 2/12/2019.
 */
public class OptionsPresenter extends BasePresenter<OptionsContract.View>
        implements OptionsContract.Presenter<OptionsContract.View> {

    private static OptionsPresenter optionsPresenter;

    private OptionsPresenter(OptionsContract.View view) {
        mView = view;
    }

    static OptionsPresenter getInstance(OptionsContract.View view) {
        if (optionsPresenter == null) {
            optionsPresenter = new OptionsPresenter(view);
        } else {
            optionsPresenter.onAttach(view);
        }
        return optionsPresenter;
    }

    /**
     * Removes all the data from shared preferences
     */
    @Override
    public void resetStatistics() {
        DataUtil.getInstance().removeStatistics();
        if (isViewAttached()) {
            mView.showMessage("Statistics reset");
        }
    }

    /**
     * Checks the number of teams. If they are less than two teams,
     * the add score functionality can't be used.
     */
    @Override
    public void checkAddScoreAvailability() {
        if (!isViewAttached()) {
            return;
        }

        if (DataUtil.getInstance().getAllTeams().size() >= 2) {
            mView.openAddScoreDialog();
        } else {
            mView.showError(ResourceUtil.getInstance().getString(R.string.please_add_at_least_two_teams));
        }
    }
}
