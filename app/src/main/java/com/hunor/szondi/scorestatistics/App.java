package com.hunor.szondi.scorestatistics;

import android.app.Application;

import com.hunor.szondi.scorestatistics.utils.DataUtil;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

/**
 * Entering point of the application. Used for initializing utils.
 * Created by Szondi Hunor on 2/12/2019.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ResourceUtil.createInstance(getApplicationContext());
        DataUtil.createInstance(getApplicationContext());
    }
}
