package com.hunor.szondi.scorestatistics.statistics.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.customviews.StatItem;
import com.hunor.szondi.scorestatistics.models.TeamModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter for statistics table recycler view in StatisticsFragment
 * Created by Szondi Hunor on 2/14/2019.
 */
public class StatRecyclerViewAdapter extends RecyclerView.Adapter {

    private ArrayList<TeamModel> data;

    public StatRecyclerViewAdapter(ArrayList<TeamModel> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.stat_recyclerview_viewholder, parent, false);
        return new StatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        StatItem item = ((StatViewHolder) holder).item;
        TeamModel currentData = data.get(position);

        buildView(item, currentData);
    }

    private void buildView(StatItem item, TeamModel currentData) {
        item.setName(currentData.getName())
                .setPlayedMatch(String.valueOf(currentData.getPlayedMatches()))
                .setWin(String.valueOf(currentData.getWins()))
                .setDraw(String.valueOf(currentData.getDraws()))
                .setLose(String.valueOf(currentData.getLoses()))
                .setScoredGoals(String.valueOf(currentData.getScoredGoals()))
                .setConcededGoals(String.valueOf(currentData.getConcededGoals()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updateData(ArrayList<TeamModel> stats) {
        this.data = stats;
        notifyDataSetChanged();
    }

    static class StatViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.statViewHolderItem)
        StatItem item;

        StatViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
