package com.hunor.szondi.scorestatistics.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.hunor.szondi.scorestatistics.models.ScoreModel;

import java.util.ArrayList;
import java.util.Map;

/**
 * Util with works with shared preferences
 * The app uses two shared preferences files:
 * - teams: here are stored the team in the order they were saved
 * - scores: here are stored the scores in the order they were saved in the following format:
 *      Key: {
 *          team1: String,
 *          team2: String,
 *          score1: Number,
 *          score2: Number
 *      }
 *
 * Created by Szondi Hunor on 2/12/2019.
 */
public class DataUtil {

    private static DataUtil dataUtil;

    private SharedPreferences sharedPreferencesTeams;
    private SharedPreferences sharedPreferencesScore;
    private SharedPreferences.Editor editor;

    private DataUtil(Context context) {
        sharedPreferencesTeams = context.getSharedPreferences("teams", Context.MODE_PRIVATE);
        sharedPreferencesScore = context.getSharedPreferences("score", Context.MODE_PRIVATE);
    }

    /**
     * Initializing DataUtil, in order to have access to shared preferences from anywhere in the app.
     * Call it only from App and only once!
     *
     * @param context Provide the application context
     */
    public static void createInstance(Context context) {
        dataUtil = new DataUtil(context);
    }

    public static DataUtil getInstance() {
        return dataUtil;
    }

    /**
     * Saves a team
     * @param value team name
     */
    public void addTeam(String value) {
        String id = String.valueOf(sharedPreferencesTeams.getAll().size() + 1);

        editor = sharedPreferencesTeams.edit();
        editor.putString(id, value);
        editor.apply();
    }

    /**
     * Returns all the teams were saved in "teams" shared preferences
     * @return list of team names
     */
    public ArrayList<String> getAllTeams() {
        ArrayList<String> list = new ArrayList<>();

        Map<String, ?> allEntries = sharedPreferencesTeams.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
            list.add(entry.getValue().toString());
        }

        return list;
    }

    /**
     * Saves a new match score
     * @param score scoreModel
     */
    public void addScore(ScoreModel score) {
        String id = String.valueOf(sharedPreferencesScore.getAll().size() + 1);
        String scoreJson = new Gson().toJson(score);

        editor = sharedPreferencesScore.edit();
        editor.putString(id, scoreJson);
        editor.apply();
    }

    /**
     * Returns all the scores were saved in "score" shared preferences
     * @return list of scoreModels
     */
    public ArrayList<ScoreModel> getAllScores() {
        ArrayList<ScoreModel> list = new ArrayList<>();

        Map<String, ?> allEntries = sharedPreferencesScore.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
            ScoreModel score = new Gson().fromJson(entry.getValue().toString(), ScoreModel.class);
            list.add(score);
        }

        return list;
    }

    /**
     * Removes all data from each shared preferences
     */
    public void removeStatistics() {
        editor = sharedPreferencesTeams.edit();
        editor.clear();
        editor.apply();

        editor = sharedPreferencesScore.edit();
        editor.clear();
        editor.apply();
    }
}
