package com.hunor.szondi.scorestatistics.base;

/**
 * General presenter extended by all the Contract.Presenter interfaces in the app.
 * Created by Szondi Hunor on 2/12/2019.
 */
public interface IPresenter<T extends IView> {

    /**
     * Attaches a view to the presenter
     * @param view IView
     */
    void onAttach(T view);

    /**
     * Detaches the view from the presenter
     */
    void onDetach();
}
