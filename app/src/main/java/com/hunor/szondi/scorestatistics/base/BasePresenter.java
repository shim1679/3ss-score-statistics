package com.hunor.szondi.scorestatistics.base;

/**
 * MVP pattern base component extended by all presenters in the app.
 * Created by Szondi Hunor on 2/12/2019.
 */
public abstract class BasePresenter<T extends IView> implements IPresenter<T> {

    protected T mView;

    @Override
    public void onAttach(T view) {
        mView = view;
    }

    @Override
    public void onDetach() {
        mView = null;
    }

    protected boolean isViewAttached() {
        return mView != null;
    }

    public T getView() {
        return mView;
    }
}
