package com.hunor.szondi.scorestatistics.options;

import com.hunor.szondi.scorestatistics.base.IPresenter;
import com.hunor.szondi.scorestatistics.base.IView;

/**
 * Creates connection between View and Presenter.
 * Created by Szondi Hunor on 2/12/2019.
 */
public interface OptionsContract {

    interface View extends IView {

        void openAddScoreDialog();
    }

    interface Presenter<T extends View> extends IPresenter<T> {

        void resetStatistics();

        void checkAddScoreAvailability();
    }
}
