package com.hunor.szondi.scorestatistics.base;

/**
 * General view extended by all the Contract.View interfaces in the app.
 * Created by Szondi Hunor on 2/12/2019.
 */
public interface IView {

    /**
     * Show a message with the given text
     * @param message text
     */
    void showMessage(String message);

    /**
     * Show a message with the given text and "Error" title
     * @param error text
     */
    void showError(String error);

}
