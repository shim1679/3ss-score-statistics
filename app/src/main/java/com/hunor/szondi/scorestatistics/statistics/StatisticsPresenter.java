package com.hunor.szondi.scorestatistics.statistics;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.base.BasePresenter;
import com.hunor.szondi.scorestatistics.models.ScoreModel;
import com.hunor.szondi.scorestatistics.models.TeamModel;
import com.hunor.szondi.scorestatistics.utils.DataUtil;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Logical part for StatisticsFragment
 * Created by Szondi Hunor on 2/12/2019.
 */
public class StatisticsPresenter extends BasePresenter<StatisticsContract.View>
        implements StatisticsContract.Presenter<StatisticsContract.View> {

    private static StatisticsPresenter statisticsPresenter;

    private ArrayList<String> teams;
    private ArrayList<ScoreModel> scores;

    private ArrayList<TeamModel> stats;

    private StatisticsPresenter(StatisticsContract.View view) {
        mView = view;
    }

    public static StatisticsPresenter getInstance(StatisticsContract.View view) {
        if (statisticsPresenter == null) {
            statisticsPresenter = new StatisticsPresenter(view);
        } else {
            statisticsPresenter.onAttach(view);
        }
        return statisticsPresenter;
    }

    @Override
    public void prepareData() {
        teams = DataUtil.getInstance().getAllTeams();
        scores = DataUtil.getInstance().getAllScores();

        //order teams by name
        Collections.sort(teams);

        buildDataForStatistics();
        buildDataForScoreTable();

    }

    @Override
    public void initSortSpinner() {
        ArrayList<String> sortOptions = new ArrayList<>();
        sortOptions.add(ResourceUtil.getInstance().getString(R.string.name));
        sortOptions.add(ResourceUtil.getInstance().getString(R.string.played_match_abr));
        sortOptions.add(ResourceUtil.getInstance().getString(R.string.win));
        sortOptions.add(ResourceUtil.getInstance().getString(R.string.draw));
        sortOptions.add(ResourceUtil.getInstance().getString(R.string.lose));
        sortOptions.add(ResourceUtil.getInstance().getString(R.string.scored_goals_abr));
        sortOptions.add(ResourceUtil.getInstance().getString(R.string.conceded_goals_abr));

        if(isViewAttached()){
            mView.initSortSpinner(sortOptions);
        }
    }

    /**
     * Builds data for the score table from the scores stored in the shared preferences
     */
    private void buildDataForScoreTable() {
        //creating a new data structure for team, for easier indexing
        HashMap<String, Integer> teamMap = new HashMap<>();
        for (int i = 0; i < teams.size(); i++) {
            teamMap.put(teams.get(i), i);
        }

        //0 all elements of the score table
        int[][] table = new int[teams.size()][teams.size()];
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                table[i][j] = 0;
            }
        }

        //calculating score for each team pair
        for (ScoreModel score : scores) {
            if(teamMap.get(score.getTeam2()) != null && teamMap.get(score.getTeam1()) != null) {
                //team1 scored score1 goals to team2
                table[teamMap.get(score.getTeam2())][teamMap.get(score.getTeam1())] += score.getScore1();

                //team2 scored score2 goals to team1
                table[teamMap.get(score.getTeam1())][teamMap.get(score.getTeam2())] += score.getScore2();
            }
        }

        //arranging the score table and team names in order to show in recycler view
        ArrayList<String> scoreTable = new ArrayList<>();
        scoreTable.add("");
        scoreTable.addAll(teams);

        int i = 0;
        for (String team : teams) {
            scoreTable.add(team);
            for (int j = 0; j < table.length; j++) {
                //if the two team are the same there will be an *
                if (i == j) {
                    scoreTable.add("*");
                } else {
                    scoreTable.add(String.valueOf(table[i][j]));
                }
            }
            i++;
        }

        if (isViewAttached()) {
            mView.initScoreTable(scoreTable, teams.size() + 1);
        }

    }

    /**
     * Builds data for the statistics table from the scores stored in the shared preferences
     */
    private void buildDataForStatistics() {
        fillStats();

        for (ScoreModel score : scores) {
            for (TeamModel stat : stats) {

                if (score.getTeam1().equals(stat.getName())) {
                    stat.increasePlayedMatches();
                    stat.addScoredGoals(score.getScore1());
                    stat.addConcededGoals(score.getScore2());

                    if (score.getScore1() > score.getScore2()) {
                        stat.increaseWins();
                    } else if (score.getScore1() == score.getScore2()) {
                        stat.increaseDraws();
                    }
                }

                if (score.getTeam2().equals(stat.getName())) {
                    stat.increasePlayedMatches();
                    stat.addScoredGoals(score.getScore2());
                    stat.addConcededGoals(score.getScore1());

                    if (score.getScore1() < score.getScore2()) {
                        stat.increaseWins();
                    } else if (score.getScore1() == score.getScore2()) {
                        stat.increaseDraws();
                    }
                }
            }
        }

        if (isViewAttached()) {
            mView.initStatisticsTable(stats);
        }
    }

    /**
     * Fills the stats field with team names
     */
    private void fillStats() {
        stats = new ArrayList<>();
        for (String teamName : teams) {
            stats.add(new TeamModel(teamName));
        }
    }

    @Override
    public void sortStatsBy(int option) {
        switch (option) {
            case 0:
                Collections.sort(stats, (o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
                break;
            case 1:
                Collections.sort(stats, (o1, o2) -> o2.getPlayedMatches() - o1.getPlayedMatches());
                break;
            case 2:
                Collections.sort(stats, (o1, o2) -> o2.getWins() - o1.getWins());
                break;
            case 3:
                Collections.sort(stats, (o1, o2) -> o2.getDraws() - o1.getDraws());
                break;
            case 4:
                Collections.sort(stats, (o1, o2) -> o2.getLoses() - o1.getLoses());
                break;
            case 5:
                Collections.sort(stats, (o1, o2) -> o2.getScoredGoals() - o1.getScoredGoals());
                break;
            case 6:
                Collections.sort(stats, (o1, o2) -> o2.getConcededGoals() - o1.getConcededGoals());
                break;
        }

        if (isViewAttached()) {
            mView.updateStatisticsTable(stats);
        }
    }
}
