package com.hunor.szondi.scorestatistics.splash;

import android.os.Bundle;
import android.os.Handler;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.utils.NavigationHelperUtil;

import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;

/**
 * First screen appearing when the app starts.
 * Created by Szondi Hunor on 2/13/2019.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        startCounter();
    }

    private void startCounter() {
        new Handler().postDelayed(() -> {
            startActivity(NavigationHelperUtil.getMainActivityIntent(SplashActivity.this));
            finish();
        }, TimeUnit.SECONDS.toMillis(2));
    }
}
