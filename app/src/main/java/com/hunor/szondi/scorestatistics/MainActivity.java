package com.hunor.szondi.scorestatistics;

import android.os.Bundle;

import com.hunor.szondi.scorestatistics.base.IOnBackPressed;
import com.hunor.szondi.scorestatistics.customviews.NavigationHeader;
import com.hunor.szondi.scorestatistics.utils.NavigationHelperUtil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Main screen which manages the two fragments of the app
 * Created by Szondi Hunor on 2/12/2019.
 */
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.navigationHeader)
    NavigationHeader navigationHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        NavigationHelperUtil.openOptionsFragment(getSupportFragmentManager(), R.id.fragmentContainer,
                navigationHeader);
    }

    @Override
    public void onBackPressed() {
        // If a fragment is active when back button is pressed,
        // let the fragment handle the back operation itself
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (fragment instanceof IOnBackPressed) {
            ((IOnBackPressed) fragment).onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}
