package com.hunor.szondi.scorestatistics.addteam;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.base.BasePresenter;
import com.hunor.szondi.scorestatistics.utils.DataUtil;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

import java.util.ArrayList;

/**
 * Logical part for AddTeamDialog
 * Created by Szondi Hunor on 2/15/2019.
 */
public class AddTeamPresenter extends BasePresenter<AddTeamContract.View>
        implements AddTeamContract.Presenter<AddTeamContract.View> {

    private static AddTeamPresenter presenter;


    private AddTeamPresenter(AddTeamContract.View view) {
        mView = view;
    }

    static AddTeamPresenter getInstance(AddTeamContract.View view) {
        if (presenter == null) {
            presenter = new AddTeamPresenter(view);
        } else {
            presenter.onAttach(view);
        }
        return presenter;
    }

    @Override
    public void saveData(String team) {
        ArrayList<String> teams = DataUtil.getInstance().getAllTeams();
        if (!teams.contains(team)) {
            DataUtil.getInstance().addTeam(team);
            if (isViewAttached()) {
                mView.closeDialog();
            }
        } else {
            if (isViewAttached()) {
                mView.showErrorLabel(ResourceUtil.getInstance().getString(R.string.team_already_exists));
            }
        }
    }
}
