package com.hunor.szondi.scorestatistics.base;

/**
 * Has to be called in activity and implemented in each fragment.
 * Created by Szondi Hunor on 2/12/2019.
 */
public interface IOnBackPressed {
    /**
     * Call the activity onBackPressed() method in general
     */
    void onBackPressed();
}
