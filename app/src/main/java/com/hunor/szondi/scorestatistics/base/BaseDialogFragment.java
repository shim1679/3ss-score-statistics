package com.hunor.szondi.scorestatistics.base;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.utils.DialogFactory;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

import androidx.fragment.app.DialogFragment;

/**
 * Custom DialogFragment extended by all the dialog fragments in the app
 * Created by Szondi Hunor on 2/13/2019.
 */
public abstract class BaseDialogFragment extends DialogFragment {

    public void showMessage(String message) {
        if (getContext() != null) {
            DialogFactory.makeMessage(getContext(), "", message).show();
        }
    }

    public void showError(String error) {
        if (getContext() != null) {
            DialogFactory.makeMessage(getContext(), ResourceUtil.getInstance().getString(R.string.error), error).show();
        }
    }
}
