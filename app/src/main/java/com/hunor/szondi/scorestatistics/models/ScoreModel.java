package com.hunor.szondi.scorestatistics.models;

import androidx.annotation.NonNull;

/**
 * Model for storing scores in the shared preferences
 * Created by Szondi Hunor on 2/13/2019.
 */
public class ScoreModel {

    private String team1;
    private String team2;
    private int score1;
    private int score2;

    public String getTeam1() {
        return team1;
    }

    public ScoreModel setTeam1(String team1) {
        this.team1 = team1;
        return this;
    }

    public String getTeam2() {
        return team2;
    }

    public ScoreModel setTeam2(String team2) {
        this.team2 = team2;
        return this;
    }

    public int getScore1() {
        return score1;
    }

    public ScoreModel setScore1(int score1) {
        this.score1 = score1;
        return this;
    }

    public int getScore2() {
        return score2;
    }

    public ScoreModel setScore2(int score2) {
        this.score2 = score2;
        return this;
    }

    @NonNull
    @Override
    public String toString() {
        return "ScoreModel{" +
                "team1='" + team1 + '\'' +
                ", team2='" + team2 + '\'' +
                ", score1=" + score1 +
                ", score2=" + score2 +
                '}';
    }
}
