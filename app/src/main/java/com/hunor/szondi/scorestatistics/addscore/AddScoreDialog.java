package com.hunor.szondi.scorestatistics.addscore;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.base.BaseDialogFragment;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Provides a UI for adding new match scores
 * Created by Szondi Hunor on 2/13/2019.
 */
public class AddScoreDialog extends BaseDialogFragment implements AddScoreContract.View {

    public static String TAG = AddScoreDialog.class.getCanonicalName();

    @BindView(R.id.addScoreDialogFirstTeam)
    Spinner firstTeamSpinner;

    @BindView(R.id.addScoreDialogSecondTeam)
    Spinner secondTeamSpinner;

    @BindView(R.id.addScoreDialogFirstScore)
    EditText firstScoreEditText;

    @BindView(R.id.addScoreDialogSecondScore)
    EditText secondScoreEditText;

    @BindView(R.id.addScoreDialogError)
    TextView error;

    private AddScoreContract.Presenter<AddScoreContract.View> mPresenter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_score_dialog, container, false);
        ButterKnife.bind(this, view);

        mPresenter = AddScorePresenter.getInstance(this);
        mPresenter.fetchTeams();

        return view;
    }

    @Override
    public void initView(ArrayList<String> teamList) {
        if (getContext() == null) {
            return;
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, teamList);
        firstTeamSpinner.setAdapter(arrayAdapter);
        secondTeamSpinner.setAdapter(arrayAdapter);

        firstTeamSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = (String) parent.getItemAtPosition(position);
                if (getContext() == null) {
                    return;
                }

                ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_list_item_1, mPresenter.getTeamListExceptOne(selected));
                secondTeamSpinner.setAdapter(arrayAdapter1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void closeDialog() {
        Toast.makeText(getContext(), getString(R.string.score_saved), Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @OnClick(R.id.addScoreDialogSaveButton)
    void onSaveClicked() {
        if (firstScoreEditText.getText().toString().isEmpty() || secondScoreEditText.getText().toString().isEmpty()) {
            error.setVisibility(View.VISIBLE);
            return;
        }

        collectData();
    }

    private void collectData() {
        String firstTeam = (String) firstTeamSpinner.getSelectedItem();
        String secondTeam = (String) secondTeamSpinner.getSelectedItem();
        int firstScore = Integer.parseInt(firstScoreEditText.getText().toString());
        int secondScore = Integer.parseInt(secondScoreEditText.getText().toString());

        mPresenter.saveData(firstTeam, secondTeam, firstScore, secondScore);
    }
}
