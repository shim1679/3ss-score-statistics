package com.hunor.szondi.scorestatistics.base;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.utils.DialogFactory;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

import androidx.fragment.app.Fragment;

/**
 * Custom Fragment extended by all the fragments in the app
 * Created by Szondi Hunor on 2/12/2019.
 */
public abstract class BaseFragment extends Fragment implements IOnBackPressed {

    public void showMessage(String message) {
        if (getContext() != null) {
            DialogFactory.makeMessage(getContext(), "", message).show();
        }
    }

    public void showError(String error) {
        if (getContext() != null) {
            DialogFactory.makeMessage(getContext(), ResourceUtil.getInstance().getString(R.string.error), error).show();
        }
    }

    public void closeFragment() {
        onBackPressed();
    }
}
