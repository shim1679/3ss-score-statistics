package com.hunor.szondi.scorestatistics.options;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hunor.szondi.scorestatistics.R;
import com.hunor.szondi.scorestatistics.base.BaseFragment;
import com.hunor.szondi.scorestatistics.customviews.NavigationHeader;
import com.hunor.szondi.scorestatistics.utils.DialogFactory;
import com.hunor.szondi.scorestatistics.utils.NavigationHelperUtil;
import com.hunor.szondi.scorestatistics.utils.ResourceUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Menu  screen of the application. Here can be initiated the following functionality:
 *  - add new team
 *  - add new match  score
 *  - show statistics of the matches
 *  - reset statistics
 * Created by Szondi Hunor on 2/12/2019.
 */
public class OptionsFragment extends BaseFragment implements OptionsContract.View {

    public static String TAG = OptionsFragment.class.getCanonicalName();

    private NavigationHeader navigationHeader;
    private int fragmentContainer;

    private OptionsContract.Presenter<OptionsContract.View> mPresenter;

    public static OptionsFragment newInstance(NavigationHeader navigationHeader, int fragmentContainer) {
        OptionsFragment fragment = new OptionsFragment();
        fragment.setNavigationHeader(navigationHeader);
        fragment.setFragmentContainer(fragmentContainer);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_option, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = OptionsPresenter.getInstance(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (navigationHeader != null) {
            navigationHeader.setTitleVisibility(View.VISIBLE)
                    .setTitle(ResourceUtil.getInstance().getString(R.string.menu))
                    .setButtonVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void openAddScoreDialog() {
        NavigationHelperUtil.openAddScoreDialog(getFragmentManager());
    }

    @OnClick(R.id.optionsAddTeamButton)
    void onAddTeamButtonClick() {
        NavigationHelperUtil.openAddTeamDialog(getFragmentManager());
    }

    @OnClick(R.id.optionsAddScoreButton)
    void onAddScoreButtonClick() {
        mPresenter.checkAddScoreAvailability();
    }

    @OnClick(R.id.optionsResetStatisticsButton)
    void onResetStatisticsButtonClick() {
        DialogFactory.makeMessage(getContext(), ResourceUtil.getInstance().getString(R.string.resetStatistics),
                ResourceUtil.getInstance().getString(R.string.reset_statistics_prompt),
                ResourceUtil.getInstance().getString(R.string.yes),
                ResourceUtil.getInstance().getString(R.string.no),
                (dialog, which) -> mPresenter.resetStatistics(), null).show();
    }

    @OnClick(R.id.optionsShowStatisticsButton)
    void onShowStatisticsButtonClick() {
        if (getActivity() != null) {
            NavigationHelperUtil.openStatisticsFragment(getActivity().getSupportFragmentManager(),
                    fragmentContainer, navigationHeader);
        }
    }

    private void setNavigationHeader(NavigationHeader navigationHeader) {
        this.navigationHeader = navigationHeader;
    }

    private void setFragmentContainer(int fragmentContainer) {
        this.fragmentContainer = fragmentContainer;
    }
}
