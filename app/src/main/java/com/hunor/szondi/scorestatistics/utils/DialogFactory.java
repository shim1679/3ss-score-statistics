package com.hunor.szondi.scorestatistics.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

import com.hunor.szondi.scorestatistics.R;

import androidx.appcompat.app.AlertDialog;

/**
 * Provides alert dialogs with different options
 * Created by Szondi Hunor on 2/12/2019.
 */
public class DialogFactory {

    public static Dialog makeMessage(Context context, String title, String text) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title)
                .setMessage(text)
                .setPositiveButton(ResourceUtil.getInstance().getString(R.string.ok), null);
        return dialog.create();
    }

    public static Dialog makeMessage(Context context, String title, String text,
                                     String positiveText, String negativetext,
                                     DialogInterface.OnClickListener positive,
                                     DialogInterface.OnClickListener negative) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(title)
                .setMessage(text)
                .setPositiveButton(positiveText, positive)
                .setNegativeButton(negativetext, negative);
        return dialog.create();
    }
}
