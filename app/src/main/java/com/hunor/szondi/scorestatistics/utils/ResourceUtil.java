package com.hunor.szondi.scorestatistics.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Helps in reaching resources from classes without context
 * Created by Szondi Hunor on 2/12/2019.
 */
public class ResourceUtil {

    private static ResourceUtil resourceUtil;
    private Context context;

    private ResourceUtil(Context context) {
        this.context = context;
    }

    /**
     * Initializing ResourceUtil, in order to have access to global resources from anywhere in the app.
     * Call it only from App and only once!
     *
     * @param context Provide the application context
     */
    public static void createInstance(Context context) {
        resourceUtil = new ResourceUtil(context);
    }

    public static ResourceUtil getInstance() {
        return resourceUtil;
    }

    public String getString(int id) {
        return context.getResources().getString(id);
    }

    public Drawable getDrawable(int id) {
        return context.getDrawable(id);
    }
}
