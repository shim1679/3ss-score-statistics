package com.hunor.szondi.scorestatistics.models;

import androidx.annotation.NonNull;

/**
 * Model for storing information about a team
 * Created by Szondi Hunor on 2/13/2019.
 */
public class TeamModel {

    private String name;
    private int playedMatches;
    private int wins;
    private int draws;
    private int scoredGoals;
    private int concededGoals;

    public TeamModel(String name) {
        this.name = name;
        playedMatches = 0;
        wins = 0;
        draws = 0;
        scoredGoals = 0;
        concededGoals = 0;
    }

    public String getName() {
        return name;
    }

    public TeamModel setName(String name) {
        this.name = name;
        return this;
    }

    public int getPlayedMatches() {
        return playedMatches;
    }

    public TeamModel setPlayedMatches(int playedMatches) {
        this.playedMatches = playedMatches;
        return this;
    }

    public TeamModel increasePlayedMatches() {
        playedMatches++;
        return this;
    }

    public int getWins() {
        return wins;
    }

    public TeamModel setWins(int wins) {
        this.wins = wins;
        return this;
    }

    public TeamModel increaseWins() {
        wins++;
        return this;
    }

    public int getDraws() {
        return draws;
    }

    public TeamModel setDraws(int draws) {
        this.draws = draws;
        return this;
    }

    public TeamModel increaseDraws() {
        draws++;
        return this;
    }

    public int getScoredGoals() {
        return scoredGoals;
    }

    public TeamModel setScoredGoals(int scoredGoals) {
        this.scoredGoals = scoredGoals;
        return this;
    }

    public TeamModel addScoredGoals(int scoredGoals) {
        this.scoredGoals += scoredGoals;
        return this;
    }

    public int getConcededGoals() {
        return concededGoals;
    }

    public TeamModel setConcededGoals(int concededGoals) {
        this.concededGoals = concededGoals;
        return this;
    }

    public TeamModel addConcededGoals(int concededGoals) {
        this.concededGoals += concededGoals;
        return this;
    }

    public int getLoses() {
        return playedMatches - wins - draws;
    }

    @NonNull
    @Override
    public String toString() {
        return "TeamModel{" +
                "name='" + name + '\'' +
                ", playedMatches=" + playedMatches +
                ", wins=" + wins +
                ", draws=" + draws +
                ", scoredGoals=" + scoredGoals +
                ", concededGoals=" + concededGoals +
                '}';
    }
}
